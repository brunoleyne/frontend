import React from 'react'
import Modal from 'react-bootstrap/lib/Modal'
import Button from 'react-bootstrap/lib/Button'


export default props => {
    const { modal, message, file } = props

    return (
        <Modal show={modal} onHide={props.close}>
            <Modal.Header closeButton>
                <Modal.Title>Confirmação</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p dangerouslySetInnerHTML={{ __html: message }}/>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.close}>Cancelar</Button>
                <Button bsStyle='primary' onClick={() => props.postConfirm(file)}>Confirmar</Button>
            </Modal.Footer>
        </Modal>
    )
}