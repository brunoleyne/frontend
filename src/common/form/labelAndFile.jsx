import React from 'react'
import Grid from '../layout/grid'
import consts from '../../consts'

export default props => {
    const style = props.style || {}    
    const preview = props.preview || false
    const className = props.className || ''
    const readOnly = props.readOnly || false

    delete props.input.value

    return(
        <Grid cols={props.cols} style={style}>
            <div className='form-group'>
                <label htmlFor={props.name}>{props.label}</label>
                {
                    readOnly ? preview ? <img src={consts.API_URL + props.filepath} style={{maxHeight: '70px', marginLeft: '11px'}}  /> : '' :
                    <input {...props.input} type={props.type} className={`${className} form-control`} /> 
                }
                {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
            </div>
        </Grid>
    )
}