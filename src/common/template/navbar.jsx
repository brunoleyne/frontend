import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { logout, editMe } from '../../auth/authActions'
import Profile from './profile'

class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = { open: false }
    }

    changeOpen() {
        this.setState({ open: !this.state.open })
    }

    render() {
        const user = this.props.user || {}
        return (
            <div className='navbar-custom-menu'>
                <ul className='nav navbar-nav'>
                    <li onMouseLeave={() => this.changeOpen()} className={`dropdown user user-menu ${this.state.open ? 'open' : ''}`}>
                        <a href='javascript:;' onClick={() => this.changeOpen()} aria-expanded={this.state.open ? 'true' : 'false'} className='dropdown-toggle' data-toggle='dropdown'>
                            <span className='hidden-xs'>{user.name}</span>
                        </a>
                        <ul className='dropdown-menu'>
                            <li className='user-header'>
                                <p>{user.name}<small>{user.email}</small></p>
                            </li>
                            <li className='user-footer'>
                                <div className='pull-left'>
                                    <a className='button yellow br-bottom-right' onClick={this.props.editMe} title='Alterar Meus Dados'>Alterar Meus Dados</a>
                                </div>
                                <div className='pull-right'>
                                    <a className='button yellow br-bottom-left' href='#' onClick={this.props.logout}>Sair</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <Profile />
            </div>
        )
    }
}

const mapStateToProps = state => ({user: state.auth.user})
const mapDispatchToProps = dispatch => bindActionCreators({ logout, editMe }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
