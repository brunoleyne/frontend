import React from 'react'
import Navbar from './navbar'
import { Link } from 'react-router'

export default props => (
    <header className='main-header'>
        <Link to='/' className='logo'>
            <span className='logo-mini'>
                <img className="img-responsive center-block" src="img/logo-small.png" />
            </span>
            <span className='logo-lg'>
                <img className="img-responsive center-block" src="img/logo.png" />
            </span>
        </Link>
        <nav className='navbar navbar-static-top'>
            <a href='#' className='sidebar-toggle' data-toggle="push-menu" role="button"></a>
            <Navbar />
        </nav>
</header>
)
