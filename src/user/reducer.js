const INITIAL_STATE = {
    list: [], totalSize: 0, page: 1, sizePerPage: 10, filters: [],
    id: '', message: '', modal: false, role: [], reset: false,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_FETCHED':
            const { items, totalSize, page, sizePerPage, filters } = action.payload.data
            return { ...state, list: items, totalSize: totalSize, page: page, sizePerPage: sizePerPage, filters: filters }
        case 'USER_RESET_MODAL_OPENED':
            const { id, message } = action.payload
            return { ...state, reset: true, id: id, message: message }
        case 'USER_MODAL_OPENED':
            var { id, message, label } = action.payload
            return { ...state, modal: true, id: id, message: message, label: label }
        case 'USER_MODAL_CLOSED':
            return { ...state, modal: false, reset: false }
        default:
            return state
    }
}