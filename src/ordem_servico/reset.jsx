import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import { close, reset } from './actions'


class Dialog extends Component {

    render() {
        const { modal, message, id } = this.props
        const loading = this.props.loading || false

        return (
            <Modal show={modal} onHide={this.props.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmação</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>{ message }</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.close}>Cancelar</Button>
                    <Button bsStyle='primary' disabled={loading} onClick={() => this.props.reset(id)}>{loading ? 'Aguarde...' : 'Enviar'}</Button>
                </Modal.Footer>
            </Modal>
        )
    }

}

const mapStateToProps = state => ({ 
    id      : state.user.id,
    modal   : state.user.reset,
    message : state.user.message,
    loading : state.auth.loading
})
const mapDispatchToProps = dispatch => bindActionCreators({ close, reset }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Dialog)