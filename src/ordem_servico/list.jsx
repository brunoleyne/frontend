import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Link } from 'react-router'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'

import Modal from './reset'

import { index } from './actions'


class List extends Component {

    constructor(props) {
        super(props)
        this.handlePageChange = this.handlePageChange.bind(this)
    }

    componentWillMount() {
        const page = this.props.page || 1
        const sizePerPage = this.props.sizePerPage || 10
        const filters = this.props.filters || []

        this.props.index(page, sizePerPage, filters)
    }

    handlePageChange(page, sizePerPage) {
        const filters = this.props.filters || []

        this.props.index(page, sizePerPage, filters)
    }

    statusFormatter(cell){
        switch (cell) {
            case 1:
                return "Aberto";
            case 2:
                return "Em Andamento";
            case 3:
                return "Resolvido";
            case 4:
                return "Sem Solução";
        }
    }

    renderLinks(cell, row, props) {
        return (
            <div className="table-options">
                <OverlayTrigger placement="top" overlay={<Tooltip id="">Editar</Tooltip>}>
                    <Link className="orange" to={{ pathname: "/ordem-servico/" + row.id + "/edit" }}>
                        <i className="fa fa-pencil"></i>
                    </Link>
                </OverlayTrigger>
            </div>
        )
    }

    renderPaginationPanel(props) {
      return (
        <div className="col-xs-12 text-right">{ props.components.pageList }</div>
      );
    }

    dateFormatter(cell){
        return new Date (cell).toLocaleDateString('en-GB')
    }
    render() {
        const options = {
            paginationPanel   : this.renderPaginationPanel,
            noDataText        : 'Não há registros para serem exibidos.', 
            hideSizePerPage   : true,
            onPageChange      : this.handlePageChange,
            page              : this.props.page,
            sizePerPage       : this.props.sizePerPage,
        }

        return (
            <div>
                <div className='table-responsive mailbox-messages'>
                    <BootstrapTable
                        keyField='id' 
                        options={ options } 
                        data={this.props.list} 
                        fetchInfo={{ dataTotalSize: this.props.totalSize }}
                        pagination remote striped hover condensed >
                        <TableHeaderColumn dataField='name' headerTitle={false}>Solicitante</TableHeaderColumn>
                        <TableHeaderColumn dataField='created_at' dataFormat={this.dateFormatter} headerTitle={false}>Criado Em</TableHeaderColumn>
                        <TableHeaderColumn dataField='updated_at' dataFormat={this.dateFormatter} headerTitle={false}>Atualizado Em</TableHeaderColumn>
                        <TableHeaderColumn dataField='description' headerTitle={false}>Descrição</TableHeaderColumn>
                        <TableHeaderColumn dataField='status' dataFormat={this.statusFormatter} headerTitle={false}>Status</TableHeaderColumn>
                        <TableHeaderColumn dataField='' dataFormat={ this.renderLinks } formatExtraData={ this.props } headerText='Ações'>Ações</TableHeaderColumn>
                    </BootstrapTable>
                </div>
                <Modal/>
            </div>
        )
    }
}

const mapStateToProps = state => ({ 
    list        : state.ordemServico.list,
    totalSize   : state.ordemServico.totalSize,
    page        : state.ordemServico.page,
    sizePerPage : state.ordemServico.sizePerPage,
    filters     : state.ordemServico.filters,
    user        : state.auth.user
})
const mapDispatchToProps = dispatch => bindActionCreators({ index }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(List)
