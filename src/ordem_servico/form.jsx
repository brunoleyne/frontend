import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, formValueSelector, reset, initialize } from 'redux-form'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import { Link } from 'react-router'
import { post, update, create, edit } from './actions'
import LabelAndSelect from '../common/form/labelAndSelect'
import LabelAndTextarea from "../common/form/labelAndTextarea";


class Form extends Component {

    componentWillMount() {
        const id = this.props.params.id || false
        id ? this.props.edit(id) : this.props.create()
    }

    render() {
        const { handleSubmit } = this.props
        const submit = this.props.params.id ? this.props.update : this.props.post
        const readOnly = (this.props.params.id && !this.props.params.edit) ? true : false
        const required = value => (value ? undefined : 'Este campo é obrigatório')

        let status = [
            { id: '1', value: 'Aberto' },
            { id: '2' , value: 'Em Andamento' },
            { id: '3' , value: 'Resolvido' },
            { id: '4' , value: 'Sem Solução' },
        ]

        return (
            <div className='container-fluid'>
                <div className='row'>
                    <ContentHeader title='Ordem Serviço'>
                        <Link to='ordem-servico' title='Voltar' className='btn btn-default pull-right'>
                            <i className='fa fa-chevron-left' aria-hidden='true'></i> Voltar
                        </Link>
                    </ContentHeader>
                    <Content>
                        <form role='form' onSubmit={handleSubmit(submit)} >
                            <div className='row'>
                                <Field name='description' component={LabelAndTextarea} label='Descrição' cols='12 12' tabIndex="4" className="br-bottom-left" validate={[required]} readOnly={readOnly}/>
                            </div>
                            <div className='row'>
                                <Field name='status' component={LabelAndSelect} options={status} label='Status*' cols='12 6' validate={[required]} combobox={false} />
                            </div>
                            { readOnly ? '' :
                                <div className="row">
                                    <div className="col-lg-12 form-group">
                                        <input type="submit" value="Salvar" className="btn btn-flat btn-primary" />
                                    </div>
                                </div> }
                        </form>
                    </Content>
                </div>
            </div>
        )
    }
}

Form = reduxForm({form: 'orderServiceForm', onSubmit: post, destroyOnUnmount: false})(Form)
const selector = formValueSelector('orderServiceForm')
const mapStateToProps = state => ({
    user     : state.auth.user
})
const mapDispatchToProps = dispatch => bindActionCreators({ post, update, create, edit }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Form)