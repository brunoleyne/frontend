const INITIAL_STATE = {
    list: [], totalSize: 0, page: 1, sizePerPage: 10, filters: [],
    id: '', message: '', modal: false, role: [], reset: false,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'ORDER_SERVICE_FETCHED':
            const { items, totalSize, page, sizePerPage, filters } = action.payload.data
            return { ...state, list: items, totalSize: totalSize, page: page, sizePerPage: sizePerPage, filters: filters }
        default:
            return state
    }
}