import React, { Component } from 'react'
import { connect } from 'react-redux'

import Header from '../common/template/header'
import SideBar from '../common/template/sidebar'
import Footer from '../common/template/footer'
import Messages from '../common/msg/messages'


class App extends Component {

    componentDidMount() {
        $(document).ready(function() {
            $('body').layout('fix')
            $('.main-sidebar ul').tree()
            $('body').removeClass().addClass('sidebar-mini')
        })
    }

    render() {
        return (
            <div className='wrapper'>
                <Header />
                <SideBar />
                <div className='content-wrapper'> 
                    {this.props.children}
                </div>
                <Footer />
                <Messages />
            </div>
        )
    }

}

export default connect(null, null)(App)
