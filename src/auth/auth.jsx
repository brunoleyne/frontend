import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {reduxForm, Field} from 'redux-form'

import Row from '../common/layout/row'
import Grid from '../common/layout/grid'
import Messages from '../common/msg/messages'
import Input from '../common/form/inputAuth'
import OnlyInput from '../common/form/onlyInput'

import {Link} from 'react-router'
import {login, goForgotPassword, backLogin, forgotPassword, resetPassword, setTokenCaptcha} from './authActions'
import {loadReCaptcha, ReCaptcha} from 'react-recaptcha-google'
import consts from "../consts";

class Auth extends Component {

    constructor(props) {
        super(props);
        this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onForgotPassword = this.onForgotPassword.bind(this);
        this.onResetPassword = this.onResetPassword.bind(this);
        this.onResetPassword = this.onResetPassword.bind(this);
    }

    onSubmit(values, captcha) {
        this.props.login(values, captcha)
    }

    onForgotPassword(values) {
        this.props.forgotPassword(values)
    }

    onResetPassword(values) {
        this.props.resetPassword(values)
    }

    componentDidMount() {
        $(document).ready(function () {
            $('body').removeClass().addClass('hold-transition login-page')
            $('.cpf').mask('000.000.000-00', {reverse: true});
        })

        loadReCaptcha();
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
    }

    onLoadRecaptcha() {
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
    }

    verifyCallback(recaptchaToken) {
        this.props.setTokenCaptcha(recaptchaToken)
    }

    render() {
        const {handleSubmit} = this.props
        const loading = this.props.loading || false
        const token = this.props.params.token || false
        const renderForgotPassword = this.props.renderForgotPassword || false

        return (
            <div className='login-box'>
                <div className='login-logo'>
                    <img className="img-responsive center-block" src="img/logo.png"/>
                </div>

                <div className='login-box-body'>
                    <p className='login-box-msg'>Bem vindo!</p>
                    <a href={`${consts.API_URL}/auth/loginsteam`}><img
                        src='https://steamcommunity-a.akamaihd.net/public/images/signinthroughsteam/sits_02.png'/></a>
                </div>
            </div>
        )

    }
}


Auth = reduxForm({form: 'authForm'})(Auth)
const mapStateToProps = (state, props) => ({
    loading: state.auth.loading,
    renderForgotPassword: state.auth.forgotPassword,
    initialValues: {token: props.params.token}
})
const mapDispatchToProps = dispatch => bindActionCreators({
    login,
    goForgotPassword,
    backLogin,
    forgotPassword,
    resetPassword,
    setTokenCaptcha
}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Auth)
